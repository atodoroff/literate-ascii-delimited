#line 320 "file.nw"
all: pdf code
pdf: file.nw
	noweave -delay -index file.nw > file.tex
	pdflatex file.tex
	bibtex file.aux
	pdflatex file.tex
	pdflatex file.tex

code: file.nw
	notangle -Rascii-delimited -L';line %L "%F"%N' file.nw > ascii-delimited.scm
	notangle -Rtesting -L';line %L "%F"%N' file.nw > testing.scm
	notangle -RMakefile -t -L'#line %L "%F"%N' file.nw > Makefile

tests: testing.scm
	guile-3.0 testing.scm

clean:
	rm *.aux *.bbl *.log *.out *.toc *.tex *.xml *.blg *-blx.bib test
