;line 90 "file.nw"
(define-library 
;line 244 "file.nw"
(ascii-delimited)
  (export decoder encoder)
  (import (scheme base)
          (scheme write)
          (scheme file))
;line 91 "file.nw"
  (begin
    
;line 101 "file.nw"
(define encoder
  (lambda (ls file-name)
    
;line 111 "file.nw"
(define encoder_internal
  (lambda (ls port)
    
;line 130 "file.nw"
(define row_encoder
  (lambda (ls port)
    
;line 147 "file.nw"
(define atom_dispatch
  (lambda (atom port)
    (cond
      ((eqv? atom '()))
      (else
        (display atom port)))))
;line 133 "file.nw"
    (cond
     ((null? ls))
     ((null? (cdr ls))
      (atom_dispatch (car ls) port))
     (else
      (atom_dispatch (car ls) port)
      (display #\us port)
      (row_encoder (cdr ls) port)))))
;line 114 "file.nw"
    (cond
     ((null? ls) (close-output-port port))
     ((null? (cdr ls))
      (row_encoder (car ls) port)
      (encoder_internal (cdr ls) port))
     (else
      (row_encoder (car ls) port)
      (display #\rs port)
      (encoder_internal (cdr ls) port)))))
;line 104 "file.nw"
    (encoder_internal ls (open-output-file file-name))))
;line 93 "file.nw"
    
;line 161 "file.nw"
(define decoder
  (lambda (file-name)
    
;line 174 "file.nw"
(define decoder_internal
  (lambda (word' row' table' port)
    
;line 195 "file.nw"
(define word
  (lambda (c word')
    (cons c word')))
;line 213 "file.nw"
(define row
  (lambda (word' row')
    (cons
      (if (null? word')
        ""
        (list->string (reverse word')))
      row')))
;line 223 "file.nw"
(define table
  (lambda (word' row' table')
    (cons (reverse (row word' row')) table')))
;line 229 "file.nw"
(define finish
  (lambda (word' row' table')
    (reverse (table word' row' table'))))
;line 177 "file.nw"
    (let ((c (read-char port)))
      (cond
        ((eof-object? c)
          (close-input-port port)
          (finish word' row' table'))
        ((eq? #\rs c)
          (decoder_internal '() '()
            (table word' row' table') port))
        ((eq? #\us c)
          (decoder_internal '()
            (row word' row') table' port))
        (else
          (decoder_internal
            (word c word') row' table' port))))))
;line 164 "file.nw"
    (decoder_internal '() '() '() (open-input-file file-name))))
;line 93 "file.nw"
               ))
