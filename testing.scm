;line 264 "file.nw"
(import (scheme base))
(add-to-load-path ".")
(import (ascii-delimited))
(display "Load Success")
(newline)
;line 276 "file.nw"
(define list_to_check
  '(
    (("")) ; empty list
    (("\"\"")) ; an actual "" entered as a string
    (("1")) ; single element list
    (("1" "2") ("")); ("") stands for an empty line
    (("1") ("2"))
    (("1" "" "a") ("b" "some text" "") (""))
    (("a b c" "1 2 3" "") ("a" "b" "c") ("") ("" "z" "some more text \n a "))
    (("a") ("") ("b c d") ("" "" ""))
   ))
;line 293 "file.nw"
(define test_function
  (lambda (list)
    (cond
      ((null? list) (display "DONE: ALL PASS") (newline))
      (else
        (encoder (car list) "test")
        (if (equal? (car list) (decoder "test"))
          (test_function (cdr list))
          (begin
            (display "FAIL FOR:") (newline)
            (write (car list))
            (newline)
            (display "Had instead:") (newline)
            (write (decoder "test"))
            (newline)))))))
;line 258 "file.nw"
(test_function list_to_check)
